From bfcef16d1cd059cc8350a94e48416adb2cdb1550 Mon Sep 17 00:00:00 2001
From: Daniele Nicolodi <daniele@grinta.net>
Date: Thu, 2 Jan 2025 13:12:07 +0100
Subject: [PATCH] Use `Optional[T]` instead of `T | None` for Python 3.9
 compatibility

While most typing annotations are consumed only by type checkers, a
handful of them provide information that can be useful at runtime. For
example, beanquery uses typing annotations for the fields of the named
tuples holding ledger directive represenations derive table column
types.

Make sure that these typing annotations can be interpreted in all
supported Python versions. This requires converting some use of
the `T | None` form to `Optional[T]`. Add a test to avoid regressions.
---
 beancount/core/data.py      | 29 +++++++++++++++--------------
 beancount/core/data_test.py | 15 +++++++++++++++
 2 files changed, 30 insertions(+), 14 deletions(-)

diff --git a/beancount/core/data.py b/beancount/core/data.py
index ef5f1ea8d..1a3f046b0 100644
--- a/beancount/core/data.py
+++ b/beancount/core/data.py
@@ -13,6 +13,7 @@
 from typing import Any
 from typing import Iterator
 from typing import NamedTuple
+from typing import Optional
 from typing import Protocol
 from typing import Union
 from typing import overload
@@ -111,7 +112,7 @@ class Open(NamedTuple):
     date: datetime.date
     account: Account
     currencies: list[Currency]
-    booking: Booking | None
+    booking: Optional[Booking]
 
 
 class Close(NamedTuple):
@@ -198,8 +199,8 @@ class Balance(NamedTuple):
     date: datetime.date
     account: Account
     amount: Amount
-    tolerance: Decimal | None
-    diff_amount: Amount | None
+    tolerance: Optional[Decimal]
+    diff_amount: Optional[Amount]
 
 
 class Posting(NamedTuple):
@@ -228,11 +229,11 @@ class Posting(NamedTuple):
     """
 
     account: Account
-    units: Amount | None
-    cost: Cost | CostSpec | None
-    price: Amount | None
-    flag: Flag | None
-    meta: Meta | None
+    units: Optional[Amount]
+    cost: Optional[Union[Cost, CostSpec]]
+    price: Optional[Amount]
+    flag: Optional[Flag]
+    meta: Optional[Meta]
 
 
 class Transaction(NamedTuple):
@@ -261,8 +262,8 @@ class Transaction(NamedTuple):
     meta: Meta
     date: datetime.date
     flag: Flag
-    payee: str | None
-    narration: str | None
+    payee: Optional[str]
+    narration: Optional[str]
     tags: frozenset[str]
     links: frozenset[str]
     postings: list[Posting]
@@ -305,8 +306,8 @@ class Note(NamedTuple):
     date: datetime.date
     account: Account
     comment: str
-    tags: frozenset[str] | None
-    links: frozenset[str] | None
+    tags: Optional[frozenset[str]]
+    links: Optional[frozenset[str]]
 
 
 class Event(NamedTuple):
@@ -419,8 +420,8 @@ class Document(NamedTuple):
     date: datetime.date
     account: Account
     filename: str
-    tags: frozenset[str] | None
-    links: frozenset[str] | None
+    tags: Optional[frozenset[str]]
+    links: Optional[frozenset[str]]
 
 
 class Custom(NamedTuple):
diff --git a/beancount/core/data_test.py b/beancount/core/data_test.py
index 632e5f523..38d0df82e 100644
--- a/beancount/core/data_test.py
+++ b/beancount/core/data_test.py
@@ -3,6 +3,7 @@
 
 import datetime
 import pickle
+import typing
 import unittest
 from datetime import date
 
@@ -434,5 +435,19 @@ def test_data_tuples_support_pickle(self):
         self.assertEqual(txn1, txn2)
 
 
+class TestAnnotations(unittest.TestCase):
+    def test_directive_typed_named_tuples(self):
+        # While most typing annotations are consumed only by type
+        # checkers, a handful of them provide information that can be
+        # useful at runtime. For example, beanquery uses typing
+        # annotations for the fields of the named tuples holding
+        # ledger directive represenations derive table column types.
+        #
+        # Verify that these annotations can interpreted.
+        for cls in data.ALL_DIRECTIVES:
+            with self.subTest(cls=cls.__name__):
+                typing.get_type_hints(cls)
+
+
 if __name__ == "__main__":
     unittest.main()
